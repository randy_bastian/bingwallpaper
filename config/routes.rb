Rails.application.routes.draw do

 	get 'page(/:title)', to: "page#index"

	resources :keyword
	get 'pics(/:keyword)', to: "pics#index"
	get 'admin/keyword/new', to: "keyword#index"
	mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
	devise_for :users
	root 'home#index'

  	get '*path' => redirect('/')
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
