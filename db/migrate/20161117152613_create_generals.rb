class CreateGenerals < ActiveRecord::Migration[5.0]
  def up
    create_table :generals do |t|
    	t.string 	"title"
    	t.text		"keyword"
    	t.text 		"description"
    	t.timestamps
      t.attachment :asset
    end
    General.create :title => "Wallpaper", :keyword => "Wallpaper HD", :description => "Wallpaper HD"
  end

  def down
  	drop_table :generals
  end
end
