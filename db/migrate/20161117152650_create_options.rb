class CreateOptions < ActiveRecord::Migration[5.0]
  def up
    create_table :options do |t|
    	t.text "header_script"
    	t.text "footer_script"
    	t.text "header_ads"
    	t.text "footer_ads"
      t.timestamps
    end
    Option.create :header_script => "", :footer_script => "", :header_ads => "", :footer_ads => ""
  end

  def down
  	drop_table :options
  end
end
