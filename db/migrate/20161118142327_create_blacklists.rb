class CreateBlacklists < ActiveRecord::Migration[5.0]
  def up
    create_table :blacklists do |t|
    	t.string 	"title"
      	t.timestamps
    end
  end

  def down
  	drop_table :blacklists
  end
end
