class KeywordController < ApplicationController
  before_action :authenticate_user!
	layout "application"
  include PicsHelper
  	def index
  		global()
  	end

  	def create
  		string_array = params[:title].split("\r\n")
  		string_array.each do |arr|
  			Keyword.create(:title => arr)
  		end

      system "rake -s sitemap:refresh RAILS_ENV=production"
  		redirect_to :action => "index"
 	end
end
