class PicsController < ApplicationController
	layout "application"
	include PicsHelper
	def index
		require 'uri'
		global()
		link = params[:keyword]
		scrape(link + " 4K & HD")

		@keyword = @web_title = link.tr("-"," ")
		@general_description 	= @web_title + " - " + @general_description
		@general_keyword 		= @web_title + "," + @general_keyword
		render("home/index")
	end
end
