class HomeController < ApplicationController
	include PicsHelper
	layout "application"
  	def index
  		global()
  		@web_title 		= "Home"
  		@keyword 		= @general_title
  		scrape(@keyword)
  	end
end
