module PicsHelper
	class Entry
		def initialize(title, image, resolusi)
			@title 	= title
			@image 	= image
			@resolusi = resolusi
		end
		attr_reader :title
		attr_reader :image
		attr_reader :resolusi
	end
	
	## Perbaiki Link
	def url_escape(url)
		require 'uri'
		URI.encode(url)
	end
	
	## Action untuk scrape website
	def scrape(mylink)
		## Get HTML
		require 'open-uri'
		link = "http://www.bing.com/images/search?q=" + url_escape(mylink) + "&go=Search&qs=bs&form=QBIR"
		doc = $redis.get(link)
		if doc.nil?
			user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2725.0 Safari/537.36"
			doc = Nokogiri::HTML(open(link, "User-Agent" => user_agent))
			$redis.set(link,doc)
		else
			doc = Nokogiri::HTML.parse(doc)
		end
		#Get Halaman
		blacklists = Blacklist.all

		entries = doc.css(".dg_u")
		@cek = entries
		@entriesArray = []
		entries.each do |entry|
			title 			= entry.css("div > a")[0]["t1"]
			resolusi 		= entry.css("div > a")[0]["t2"]
			## Get image url
			raw_image		= entry.css("div > a")[0]["m"]
			image 			= raw_image.split("imgurl:\"")[1].split("\"")[0]

			## Check blacklist keyword
			flag = 0
			blacklists.each do |lists|
				if image.include? lists.title
					flag = 1
					break
				end
			end
			next if flag == 1
			## end check
			@entriesArray << Entry.new(title, image,resolusi)
		end
	end

	def global
		## SEtting global Header untuk SEO
		general 			= General.first
		@general_title 		= general.title
		@general_keyword 	= general.keyword 
		@general_description = general.description
		@general_image 		= general.asset.url
		@general_meta_image = general.asset.url
		## Setting category untuk header
		@allcategory 		= Category.all.sort_by {|h| -h[:title]}
		## setting Header,Footer Ads & Script
		options 			= Option.first
		@header_script 		= options.header_script
		@header_ads 		= options.header_ads
		@footer_script 		= options.footer_script
		@footer_ads 		= options.footer_ads
	end
end
